# BTH Web Introduction demo Project
This source code repository contains a demo application for the BTH Web Introduction presentation.

## Backend
1. Download [Java 8 JDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html)

2. Download [eclipse](http://www.eclipse.org/downloads/packages/eclipse-ide-java-developers/marsr)

3. Startup eclipse, install the *gradle* plugin: 

    Help -> Eclipse Market Place -> Gradle Integration for Eclipse by Pivotal

4. Import backend project:

    1. File -> Import -> Gradle -> Gradle Project

    2. In Root folder, enter the path to the backend folder. 

    3. Click [Build Model]. 

    4. Mark the checkbox "backend"

    5. Click finish

5. Launch configuration

    1. Run -> Run Configurations...

    2. Pick Java Application in left hand list

    3. Click "New launch configuration" in Toolbar

    4. On Tab "Main", enter Main class: `se.softhouse.bthwebintro.BTHWebIntroApplication`

    5. On Tab "Arguments", enter `server bthwebintroduction.yml` in Program arguments field.

    6. Click "Run"